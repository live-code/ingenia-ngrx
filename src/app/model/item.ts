export interface Item {
  id?: number;
  name: string;
  catId: number;
}
