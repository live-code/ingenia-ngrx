import { createAction, props } from '@ngrx/store';
import { ConfigState } from './config.reducer';

export const setConfig = createAction(
  '[config] set',
  props<{ config: Partial<ConfigState> }>()
);

export const setTheme = createAction(
  '[config] set theme',
  props<{ theme: string }>()
);

/*
dispatch(setConfig({ config: { theme: 'dark', language: 'it}}))
dispatch(setConfig({ config: { language: 'it}}))
dispatch(setConfig({ config: { theme: 'dark'}}))

dispatch(setTheme({ theme: 'dark '}))*/
