import { createReducer, on } from '@ngrx/store';
import * as ConfigActions from './config.actions';

export interface ConfigState {
  theme: string;
  language: string;
}

const initialState: ConfigState = {
  theme: 'light',
  language: 'it'
};

export const configReducer = createReducer(
  initialState,
  on(ConfigActions.setTheme, (state, action) => ({ ...state, theme: action.theme})),
  on(ConfigActions.setConfig, (state, action) => ({ ...state, ...action.config})),
)
