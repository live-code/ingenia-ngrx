import { AppState } from '../../app.module';
import { createSelector } from '@ngrx/store';

export const getConfig = (state: AppState) => state.config;

export const getTheme = createSelector(getConfig, c => c.theme);
export const getLanguage = createSelector(getConfig, c => c.language);
