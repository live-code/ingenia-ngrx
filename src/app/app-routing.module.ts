import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: 'catalog', loadChildren: () => import('./features/items/catalog.module').then(m => m.CatalogModule) }, { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
