import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { addItem, deleteItem, loadItems, loadItemsFailed } from './store/items/items.actions';
import { Observable } from 'rxjs';
import { Item } from '../../model/item';
import { Actions } from '@ngrx/effects';
import { getItems, getItemsError, getItemsFiltered } from './store/items/items.selectors';
import { Filter } from './store/filters/items-filters.reducers';
import { getAllFilters } from './store/filters/items-filters.selectors';
import { changeFilters } from './store/filters/items-filters.actions';

@Component({
  selector: 'ing-items',
  template: `
    <h1>Hello NGRX</h1>
    <div *ngIf="error$ | async">errore</div>

    <ing-item-form (saveItem)="addItemHandler($event)"></ing-item-form>
    
    <hr>
    
    <select (change)="setFilterHandler($event)">
      <option [ngValue]="null"> Select a filter</option>
      <option *ngFor="let f of getAllFilters$ | async" [value]="f.id">
        {{f.label}}
      </option>
    </select>
    
    <li *ngFor="let item of items$ | async">
      {{item.name}} {{item.id}}
      <button (click)="deleteItemHandler(item.id)">del</button>
    </li>
  `,
})
export class CatalogComponent {
  items$: Observable<Item[]> = this.store.select(getItemsFiltered);
  error$: Observable<boolean> = this.store.select(getItemsError);
  getAllFilters$: Observable<Filter[]> = this.store.select(getAllFilters);
  error: boolean;

  constructor(private store: Store, private action$: Actions) {
    this.store.dispatch(loadItems());
  }

  deleteItemHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }

  addItemHandler(item: Item): void {
    this.store.dispatch(addItem({ item }));
  }

  setFilterHandler(event: Event): void {
    const catId = +(event.currentTarget as HTMLSelectElement).value;
    this.store.dispatch(changeFilters({ catId }))

  }
}
