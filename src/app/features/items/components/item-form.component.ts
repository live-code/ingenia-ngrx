import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Item } from '../../../model/item';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { addItemSuccess, loadItems } from '../store/items/items.actions';

@Component({
  selector: 'ing-item-form',
  template: `
    <input type="text" (keydown.enter)="addItemHandler()" #input>

  `,
  styles: [
  ]
})
export class ItemFormComponent {
  @ViewChild('input') input: ElementRef<HTMLInputElement>;
  @Output() saveItem: EventEmitter<Item> = new EventEmitter<Item>();

  constructor(private action$: Actions) {
    this.action$
      .pipe(
        ofType(addItemSuccess)
      )
      .subscribe(() => {
        this.input.nativeElement.value = '';
      });
  }

  addItemHandler(): void {
    this.saveItem.emit({ name: this.input.nativeElement.value, catId: 1001});
  }

}
