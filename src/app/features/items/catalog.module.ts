import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { ItemFormComponent } from './components/item-form.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { itemsReducer, ItemsState } from './store/items/items.reducer';
import { itemsFiltersReducers, ItemsFiltersState } from './store/filters/items-filters.reducers';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items/items.effects';

export interface CatalogState {
  items: ItemsState;
  filters: ItemsFiltersState;
}

export const reducers: ActionReducerMap<CatalogState> = {
  items: itemsReducer,
  filters: itemsFiltersReducers
};

@NgModule({
  declarations: [
    CatalogComponent,
    ItemFormComponent
  ],
  exports: [
    CatalogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CatalogRoutingModule,
    StoreModule.forFeature('catalog', reducers),
    EffectsModule.forFeature([ ItemsEffects ])

  ]
})
export class CatalogModule { }
