import { createReducer, on } from '@ngrx/store';
import { loadItemsSuccess, addItem, deleteItem, addItemSuccess, deleteItemSuccess, loadItemsFailed, addItemFailed, deleteItemFailed } from './items.actions';
import { Item } from '../../../../model/item';

// const initialState: Item[] = [];
//
// export const itemsReducer = createReducer(
//   initialState,
//   on(loadItemsSuccess, (state, action) => action.items),
//   on(addItemSuccess, (state, action) => [...state, action.item]),
//   on(deleteItemSuccess, (state, action) => state.filter(item => item.id !== action.id))
// );

export interface ItemsState {
  list: Item[];
  error: boolean;
}

const initialState: ItemsState = {
  list: [],
  error: false,
};

export const itemsReducer = createReducer(
  initialState,
  on(loadItemsSuccess, (state, action) => ({ list: action.items, error: false })),
  on(addItemSuccess, (state, action) => ({
    list: [...state.list, action.item],
    error: false,
  })),
  on(deleteItemSuccess, (state, action) => ({
    list: state.list.filter(item => item.id !== action.id),
    error: false
  })),
  on(loadItemsFailed, (state, action) => ({ ...state, error: true })),
  on(addItemFailed, (state, action) => ({ ...state, error: true })),
  on(deleteItemFailed, (state, action) => ({ ...state, error: true }))
);
