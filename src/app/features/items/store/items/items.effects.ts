import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, filter, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import * as ItemActions from './items.actions';
import { combineLatest, of } from 'rxjs';
import { ItemsService } from './items.service';
import { Store } from '@ngrx/store';
import { getLanguage, getTheme } from '../../../../core/store/config.selectors';
import { deleteItemSuccess } from './items.actions';

@Injectable()
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(ItemActions.loadItems),
    mergeMap(
      () => this.itemsService.loadItems()
        .pipe(
          map(items => ItemActions.loadItemsSuccess({ items })),
          catchError(() => of(ItemActions.loadItemsFailed()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemActions.deleteItem),
    withLatestFrom(combineLatest([
      this.store.select(getTheme),
      this.store.select(getLanguage),
    ])),
    mergeMap(
      ([action, values]) => {
        console.log(values)
        return this.itemsService.deleteItem(action.id)
          .pipe(
            switchMap(() => [
              ItemActions.deleteItemSuccess({id: action.id }),
              // ItemActions.loadItems(),
            ]),
            catchError(() => of(ItemActions.deleteItemFailed()))
          );
      }
    )
  ));

  deleteItemSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(deleteItemSuccess),
    map(() => ItemActions.loadItems())
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemActions.addItem),
    withLatestFrom(this.store.select(getTheme)),
    mergeMap(
      ([action, theme]) => this.itemsService.addItem(action.item, theme)
        .pipe(
          map(item => ItemActions.addItemSuccess({ item })),
          catchError(() => of(ItemActions.addItemFailed()))
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private itemsService: ItemsService,
    private store: Store
  ) {
  }
}
