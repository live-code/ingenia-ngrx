import { createAction, props } from '@ngrx/store';
import { Item } from '../../../../model/item';


export const loadItems = createAction('[Items] load');
export const loadItemsSuccess = createAction(
  '[Items] load success',
  props<{ items: Item[]}>()
);
export const loadItemsFailed = createAction('[Items] load failed');

export const addItem = createAction(
  '[items] Add',
  props<{ item: Item }>()
);

export const addItemSuccess = createAction(
  '[items] Add Success',
  props<{ item: Item }>()
);
export const addItemFailed = createAction('[Items] add failed');

export const deleteItem = createAction(
  '[items] Delete',
  props<{ id: number }>()
);

export const deleteItemSuccess = createAction(
  '[items] Delete Success',
  props<{ id: number }>()
);
export const deleteItemFailed = createAction('[Items] delete failed');
