import { Injectable } from '@angular/core';
import { Item } from '../../../../model/item';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class ItemsService {
  loadItems(): Observable<Item[]> {
    return this.http.get<Item[]>('http://localhost:3000/items')
  }

  deleteItem(id: number): Observable<void> {
    return this.http.delete<void>(`http://localhost:3000/items/${id}`)
  }

  addItem(item: Item, theme: string): Observable<Item> {
    console.log('THEME', theme)
    return this.http.post<Item>(`http://localhost:3000/items/`, item)
  }

  constructor(private http: HttpClient) {
  }
}
