import { AppState } from '../../../../app.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CatalogState } from '../../catalog.module';
import { getCatIdFilter } from '../filters/items-filters.selectors';

export const getCatalogFeature = createFeatureSelector<CatalogState>('catalog');

export const getItems = createSelector(
  getCatalogFeature,
  (state) => state.items.list
)

export const getItemsError = createSelector(
  getCatalogFeature,
  (state) => state.items.error
)

export const getItemsFiltered = createSelector(
  getItems,
  getCatIdFilter,
  (items, catId) => {
    if (!catId) {
      return items;
    }
    return items.filter(item => item.catId === catId)
  }
)
