import { createReducer, on } from '@ngrx/store';
import { changeFilters } from './items-filters.actions';

export interface Filter {
  id: number;
  label: string;
}

export interface ItemsFiltersState {
  catId: number;
  list: Filter[];
}

const initialState = {
  catId: null,
  list: [
    { id: 1000, label: 'filtro 1000'},
    { id: 1001, label: 'filtro 1001'},
  ]
}
export const itemsFiltersReducers = createReducer(
  initialState,
  on(changeFilters, (state, action) => ({
    ...state,
    catId: action.catId
  }))
)
