import { createAction, props } from '@ngrx/store';

export const changeFilters = createAction(
  '[items filters] change',
  props<{ catId: number }>()
);
