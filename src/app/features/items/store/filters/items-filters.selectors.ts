import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CatalogState } from '../../catalog.module';

export const getCatalogFeature = createFeatureSelector<CatalogState>('catalog');

export const getAllFilters = createSelector(
  getCatalogFeature,
  state => state.filters.list
);

export const getCatIdFilter = createSelector(
  getCatalogFeature,
  state => state.filters.catId
)
