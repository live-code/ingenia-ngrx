import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { setConfig, setTheme } from '../../core/store/config.actions';

@Component({
  selector: 'ing-home',
  template: `
    <button (click)="setThemeHandler()">dark</button>
  `,
  styles: [
  ]
})
export class HomeComponent {

  constructor(private store: Store<AppState>) {}


  setThemeHandler(): void {
    this.store.dispatch(setTheme({ theme: 'dark'}));
    this.store.dispatch(setConfig({ config: { language: 'en' }}));
  }
}
