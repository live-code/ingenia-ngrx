import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { itemsReducer, ItemsState } from './features/items/store/items/items.reducer';
import { Item } from './model/item';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './features/items/store/items/items.effects';
import { ItemFormComponent } from './features/items/components/item-form.component';
import { configReducer, ConfigState } from './core/store/config.reducer';
import { environment } from '../environments/environment';

export interface AppState {
  config: ConfigState;
}

export const reducers: ActionReducerMap<AppState> = {
  config: configReducer
};

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    environment.production ? null : StoreDevtoolsModule.instrument({
      maxAge: 12
    }),
    EffectsModule.forRoot([ ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
