import { Component, ElementRef, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { addItem, addItemFailed, addItemSuccess, deleteItem, loadItems, loadItemsFailed } from './features/items/store/items/items.actions';
import { Observable } from 'rxjs';
import { Item } from './model/item';
import { getItems, getItemsError } from './features/items/store/items/items.selectors';
import { Actions, ofType } from '@ngrx/effects';
import { filter } from 'rxjs/operators';
import { AppState } from './app.module';
import { getConfig, getTheme } from './core/store/config.selectors';
import { ConfigState } from './core/store/config.reducer';

@Component({
  selector: 'ing-root',
  template: `
    <div>
      {{theme$ | async}}
      {{(config$ | async).language}}
      <button routerLink="home">home</button>
      <button routerLink="catalog">catalog</button>
    </div>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  theme$: Observable<string> = this.store.select(getTheme)
  config$: Observable<ConfigState> = this.store.select(getConfig)
  constructor(private store: Store<AppState>) {}

}
